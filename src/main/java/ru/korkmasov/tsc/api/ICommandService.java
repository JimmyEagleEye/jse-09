package ru.korkmasov.tsc.api;

import ru.korkmasov.tsc.model.Command;
import ru.korkmasov.tsc.service.CommandService;

public interface ICommandService {

    Command[] getTerminalCommands();
}
