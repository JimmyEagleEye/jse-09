package ru.korkmasov.tsc.bootstrap;

import ru.korkmasov.tsc.api.ICommandController;
import ru.korkmasov.tsc.api.ICommandService;
import ru.korkmasov.tsc.api.ICommandRepository;
import ru.korkmasov.tsc.constant.ArgumentConst;
import ru.korkmasov.tsc.constant.TerminalConst;
import ru.korkmasov.tsc.controller.CommandController;
import ru.korkmasov.tsc.repository.CommandRepository;
import ru.korkmasov.tsc.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController((ICommandService) commandService);

    public void run(final String[] args)
    {
        //displayWelcome();
        System.out.println("** WELCOME TO TASK MANAGER **");
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        if (args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

    public void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            commandController.displayWait();
            command = scanner.nextLine();
            parseCommand(command);
        }
    }

    public void showIncorrectArgument() {
        System.out.println("Error! Argument not found!");
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found!");
    }

}
