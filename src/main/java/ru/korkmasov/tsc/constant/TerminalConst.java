package ru.korkmasov.tsc.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_VERSION = "version";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_INFO = "info";

    public static final String CMD_COMMANDS = "commands";

    public static final String CMD_ARGUMENTS = "arguments";

}
