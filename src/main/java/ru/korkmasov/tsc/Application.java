package ru.korkmasov.tsc;

import ru.korkmasov.tsc.bootstrap.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
