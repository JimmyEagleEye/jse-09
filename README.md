# TASK MANAGER

# DEVELOPER INFO

NAME: Djalal Korkmasov

E-MAIL: dkorkmasov@tsconsulting.com

# SOFTWARE

* JDK 15.1

* Windows 10

# HARDWARE

* RAM 16GB

* CPU i7

* HDD 250Gb

# BUILD PROGRAM

...
mvn clean install
...

# RUN PROGRAM

java -jar ./jse-09.jar

# SCREENSHOTS

In doc folder

